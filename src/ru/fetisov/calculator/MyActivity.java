package ru.fetisov.calculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MyActivity extends Activity implements View.OnClickListener {
    /**
     * Called when the activity is first created.
     */
    public EditText num_1;
    public EditText num_2;
    public Button btn_add;
    public Button btn_sub;
    public Button btn_mult;
    public Button btn_div;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        num_1=(EditText)findViewById(R.id.num_1);
        num_2=(EditText)findViewById(R.id.num_2);
        btn_add = (Button) findViewById(R.id.btn_add);
        btn_sub = (Button) findViewById(R.id.btn_sub);
        btn_mult = (Button) findViewById(R.id.btn_mult);
        btn_div = (Button) findViewById(R.id.btn_div);
        btn_add.setOnClickListener(this);
        btn_sub.setOnClickListener(this);
        btn_mult.setOnClickListener(this);
        btn_div.setOnClickListener(this);
    }
    // проверка заполнения
    private boolean isValidate(){
        boolean flag=true;
        if(num_1.getText().length()==0){
            num_1.setError(getString(R.string.error1));
            flag=false;
        }
        if(num_2.getText().length()==0){
            num_2.setError(getString(R.string.error1));
            flag=false;
        }
        if(num_1.getText().toString().equals("-")){
            num_1.setError(getString(R.string.error2));
            flag=false;
        }
        if(num_2.getText().toString().equals("-")){
            num_2.setError(getString(R.string.error2));
            flag=false;
        }
        return flag;

    }

    @Override
    public void onClick(View v) {
        if(!isValidate()) {
            return;
        }
        float num1;
        num1 = 0;
        float num2 = 0;
        float result = 0;
        String opera="";
           // читаем EditText и заполняем переменные числами
        num1 = Float.parseFloat(num_1.getText().toString());
        num2 = Float.parseFloat(num_2.getText().toString());

        // определяем нажатую кнопку и выполняем соответствующую операцию
        // в oper пишем операцию, потом будем использовать в выводе
        switch (v.getId()) {
            case R.id.btn_add:
                opera = "+";
                result = num1 + num2;
                break;
            case R.id.btn_sub:
                opera = "-";
                result = num1 - num2;
                break;
            case R.id.btn_mult:
                opera = "*";
                result = num1 * num2;
                break;
            case R.id.btn_div:
                opera = "/";
                result = num1 / num2;
                break;
            default:
                break;
        }

        // формируем строку вывода
        String resultstr;
        if(num2<0){
            resultstr = num1 + " " + opera + " (" + num2 + ") = " + result;
        } else {
            resultstr = num1 + " " + opera + " " + num2 + " = " + result;
        }
        // запуцскаем вторую активность и передаем результат
        Intent intent = new Intent(this, EndActivity.class);
        intent.putExtra("resultstr", resultstr);
        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


}

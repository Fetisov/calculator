package ru.fetisov.calculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Алексей on 04.02.2015.
 */
public class EndActivity extends Activity{

    TextView tvresult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.end);
        
        tvresult = (TextView)findViewById(R.id.result);

        // получаем результат из первой активности
        Intent intent = getIntent();
        String resultstr = intent.getStringExtra("resultstr");

        // выводим результат
        tvresult.setText(resultstr);
    }
}